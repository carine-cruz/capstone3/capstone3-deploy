import {Container, Row, Col, Button} from 'react-bootstrap';

export default function Error(){
	return(
		<Container fluid className="welcome min-vh-100">
			
			<Row className="my-auto welcome loginRow">
				<Col className="my-auto ml-5" md={5} xs={12}>
					<h1 className="">Error</h1>
					<h4>The page you are acessing does not exist.</h4	>
					<Button variant="light" href="/" className="m-2 mt-4">Back</Button>
				</Col>
			</Row>
		</Container>
	)
}