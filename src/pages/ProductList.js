import {Container, Row, Col, Button, Table, Spinner} from 'react-bootstrap';
import {useState, useEffect, useContext } from 'react';
import { useParams, Link, useNavigate } from 'react-router-dom';
// import ReactPaginate from 'react-paginate';

import Sidebar from '../components/Sidebar';
import UserContext from '../UserContext';
import toastr from 'toastr';
import BootstrapSwitchButton from 'bootstrap-switch-button-react'

const admin = localStorage.getItem('admin');
const firstName = localStorage.getItem('firstName');

export default function ProductList(){

	// const [products, setProducts] = useState([]);
	const [breads, setBreads] = useState([]);
	const [cakes, setCakes] = useState([]);
	const [bites, setBites] = useState([]);
	const [cheesecakes, setCheesecakes] = useState([]);
	const [pies, setPies] = useState([]);
	const [products, setProducts] = useState([]);

	const [isLoading, setIsLoading] = useState(false);

	const [keyword, setKeyword] = useState("");

	//variables for pagination
	// const [currentPage, setCurrentPage] = useState(0);
	// const [itemsPerPage, setItemsPerPage] = useState(0);

	const navigate = useNavigate();
	const {category} = useParams();
	const { state, dispatch } = useContext(UserContext);

	// const screenSize = window.innerWidth;

	//retrieve products upon page load
	useEffect(()=>{

		if (admin === "true"){
			dispatch({type:"ADMIN", name: firstName, admin: true});
		} else if (admin === "false") {
			dispatch({type:"USER", name: firstName, admin: false});
		}

		if (state.admin === true){
			fetchData();
		} else {
			navigate(`../products`);
		}

		// if (screenSize <= 1440){
		// 	setItemsPerPage(5);
		// } else if (screenSize > 1440 && screenSize < 2560 ){
		// 	setItemsPerPage(8);
		// } else {
		// 	setItemsPerPage(16);
		// }

	},[category])

	const fetchData = () => {

		setIsLoading(true);
		setProducts([]);
		setKeyword("");
		
		// fetch(`http://localhost:3008/api/products/all-products`, {
		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/products/all-products`, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(result => {

			let temp = [];

			//set array for breads
			result.forEach(product => {
				if(product.category.toLowerCase() === `bread`){
					temp.push(generateTableContents(product))
				}
			});

			setBreads(temp);
			temp = [];

			//set array for pies
			result.forEach(product=>{
				if(product.category.toLowerCase() === `pies`){
					temp.push(generateTableContents(product))
				}
			});
			setPies(temp);
			temp = [];

			//set array for cheesecakes
			result.forEach(product=>{
				if(product.category.toLowerCase() === `cheesecakes`){
					temp.push(generateTableContents(product))
				}
			});

			setCheesecakes(temp);
			temp = [];

			//set array for cakes
			result.forEach(product=>{
				if(product.category.toLowerCase() === `cakes`){
					temp.push(generateTableContents(product))
				}
			})
			setCakes(temp);
			temp = [];

			//set array for quick bites
			result.forEach(product=>{
				if(product.category.toLowerCase() === `quick bites`){
					temp.push(generateTableContents(product))
				}
			})
			setBites(temp);
			temp=[];

			setIsLoading(false);

		}) // END THEN

	}

	const generateTableContents = (product) => {
		return(
			<tr key={product._id}>
				<td style={{display:'none'}}>{product._id}</td>
				<td>{product.name}</td>
				<td>&#8369; {product.price.toFixed(2)}</td>
				<td>{product.stockCount}</td>
				<td>{product.quantitySold}</td>
				<td>
					<BootstrapSwitchButton
						checked={product.isActive}
						onlabel="Yes"
						offlabel="No"
						onChange={() => updateStatus(product._id, product.stockCount)}
					/>
				</td>
				<td>
					<BootstrapSwitchButton
						checked={product.isFeatured}
						onlabel="Yes"
						offlabel="No"
						onChange={() => updateFeatured(product._id)}
					/>
				</td>
				<td>
					<Button 
						className="tableBtn" 
						onClick={()=>{
							navigate(`../update-product/${product._id}`)}}
					>Update</Button>

					<Button 
						className="tableBtn ml-2" 
						onClick={()=>{
							navigate(`../product/${product._id}`)}}
					>View</Button>					
				</td>
			</tr>
		)
	}

	//function to update featured tag
	const updateFeatured = (productId) => {
		
		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/products/update-featured/${productId}`, {
			method: "PATCH",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(result => {
			if(result){
				toastr.success(`Product is featured.`)
				fetchData();
			} else {
				toastr.error(result.message);
			}
		})
	}

	//function to update product status
	const updateStatus = (productId, qty) => {
	
		if (qty <= 0){
			toastr.error(`Cannot activate products that are out of stock!`)
		} else {
			fetch(`https://tranquil-refuge-66470.herokuapp.com/api/products/active-status/${productId}`, {
				method: "PATCH",
				headers: {
					"Authorization": `Bearer ${localStorage.getItem('token')}`
				}
			})
			.then(result => result.json())
			.then(result => {
				if(result){
					toastr.success(`Successfully updated product.`)
					fetchData();
				} else {
					toastr.error(result.message);
				}
			})
		}
	}

	const getTitle = () => {
		switch(category){
			case `breads`: return `BREAD INVENTORY`;
							// break;
			case `pies`: return `PIES INVENTORY`;
							// break;
			case `bites`: return `QUICK BITES INVENTORY`;
							// break;
			case `cakes`: return `CAKES INVENTORY`;
							// break;
			case `cheesecakes`: return `CHEESECAKES INVENTORY`;
							// break;
			default: return `CAKES INVENTORY`;
		}
	}

	const getDisplay = () => {
		switch(category){
			case `breads`: return breads;
							// break;
			case `pies`: return pies;
							// break;
			case `bites`: return bites;
							// break;
			case `cakes`: return cakes;
							// break;
			case `cheesecakes`: return cheesecakes;
									// break;
			default: return cakes;
		}
	}

	const search = () => {

		let tempCategory = category[0].toUpperCase() + category.slice(1);

		if (category === `bites`){
			tempCategory = `Quick Bites`
		} else if (category === `breads`){
			tempCategory = `Bread`
		}

		setIsLoading(true);

		fetch(`https://tranquil-refuge-66470.herokuapp.com/api/products/admin-search/${tempCategory}/keyword/${keyword}`, {
			method: "GET",
			headers: {
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(result => result.json())
		.then(result => {

			if (result.length > 0){
				setProducts(result.map(product=>generateTableContents(product)))
			} else {
				setProducts([
					<tr>
						<td colSpan="8">No results found</td>
					</tr>
				]);
			}

			setIsLoading(false);

		})
	}

	// const handlePageClick = ({selected: selectedPage}) => {
	// 	setCurrentPage(selectedPage);
	// }

	// const offset = currentPage * itemsPerPage;

	//total pages
	// const pageCount = Math.ceil(getDisplay().length / itemsPerPage);

	//slice array to display number of items per page
	// const currentPageData = getDisplay().slice(offset, offset + itemsPerPage)

	//display page --------------------------------------------
	return(
		<Container fluid className="adminPage" style={{overflowY:'auto'}}>
			<Row>
				<Col md={2} className="sideColumn ">
					<Sidebar indicator={`admin`}/>
				</Col>

				<Col md={10}>
					<div style={{height:60}}/>
					<h3><span className="loginLabel tableHeader fadeInElements">{getTitle()}</span></h3>
					<div className="text-right">
						<input 
							type="text"
							className="login fadeInElements d-inline form-control"
							style={{width:'300px'}}
							placeholder="Enter keyword"
							value={keyword}
							onChange={(e)=>setKeyword(e.target.value)}
						/>
						<Button 
							className="ml-2 fadeInElements"
							onClick={()=>search()}
						>Search</Button>
						<Button 
							className="ml-2 fadeInElements"
							onClick={()=>fetchData()}
						>Display All</Button>
						<Link 
							to={`../new-product`} 
							className="btn my-3 fadeInElements ml-2 btn-light"
						>Add new product</Link>
					</div>
					{
						isLoading === true ?

						<div className="mx-auto my-5">
							<Spinner
						    	as="span"
						    	animation="border"
						    	size="sm"
						    	role="status"
						    	aria-hidden="true"
						   		variant="light"
							/><span className="loading"> Retrieving list</span></div>

						: 
						<Table className="tableBackground fadeInElements" responsive >
							<thead>
								<tr className="tableHeader">
									<th style={{display:'none'}}>ID</th>
									<th>Name</th>
									<th>Price</th>
									<th>Stocks</th>
									<th># Sold</th>
									<th>Display</th>
									<th>Featured</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								{
									products.length > 0 ?
									products
									:getDisplay()
									/*currentPageData*/
								}
							</tbody>
						</Table>
					}

{/*					<center><ReactPaginate
						previousLabel={"<"}
						nextLabel={">"}
						pageCount={pageCount}
						containerClassName="pagination"
						onPageChange={()=>handlePageClick}
						pageClassName={"ml-1 px-2 blackOpaque"}
						nextClassName={"ml-1 px-2 whiteOpaque"}
						previousClassName={"ml-1 px-2 whiteOpaque"}
						previousLinkClassName={"sideLink"}
						nextLinkClassName={"sideLink"}
						disabledLinkClassName={"ml-1 disabledPageLink"}
						activeClassName={"tableHeader"}
					/></center>*/}

				</Col>
			</Row>	
		
		</Container>
	)

}