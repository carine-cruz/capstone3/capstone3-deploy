import {InputGroup, Button, FormControl} from 'react-bootstrap';
import { useState } from 'react';



export default function QuantitySelector({getQuantity}){

	const [qty, setQty] = useState(1);

	const addQty = (e) => {
		console.log('add')
		setQty(qty + 1);
		console.log(`value after add:`, qty);
		// passValue();
	}

	const decQty = () => {
		console.log('decrease')
		if ( qty > 1 ){
			setQty(qty - 1);
			// passValue();
		}
	}

	// getQuantity = () =>{
	// 	console.log(`child pass value`);
	// 	console.log(`value before pass:`, qty)
	// 	// getQuantity(qty)
	// 	return qty;
	// }

	return (
		<>
		<InputGroup className="mb-3 cartQty">
		  <Button 
		  	variant="light" 
		  	id="btnDec"
		  	onClick={()=>decQty()}
		  >
		    -
		  </Button>
		  <FormControl
		    className="login cartNumberField"
		    type="number"
		    value={qty}
		    id="qtyValue"
		    onChange={(e)=> setQty(e.target.value)}
		  />
		  <Button 
		  	variant="light" 
		  	id="btnAdd"
		  	onClick={()=>addQty()}
		  >
		    +
		  </Button>
		</InputGroup>
		<Button onClick={()=>getQuantity(qty)}>Pass to Parent</Button>
		</>
	)
}