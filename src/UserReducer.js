
export const initialState = {admin: null, firstName: null};

export const reducer = (state, action) =>{

	//user is logged in
	if (action.type === "ADMIN"){
		// return action.payload;
		return {admin: true, firstName: action.name}
	} else if (action.type === "USER"){
		return{admin:false, firstName:action.name}
	} else if (action.type === "NULL"){
		return {admin: null, firstName: null};
	}
}
